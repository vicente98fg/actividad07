//Generar los arrays y variables

var op = Array("+","-");
var random1 = op[op.length-1];
var random2 = op[op.length-2];


//Generamos los numeros

var num1 = +Math.floor((Math.random() * 10) + 1);
var num2 = +Math.random();
var num3 = +Math.floor((Math.random() * 10) + 1);


//Función seleccionar signo numerico

var opcion1 = (`${num1} + ${num3}`);
var opcion2 = (`${num1} - ${num3}`); 

function operador(opcion1,opcion2) {
    if (num2 > 0.5) {
        return opcion1;
    } else if (num2 <= 0.5) {
        return opcion2;
    }
}

var operacion = operador(opcion1,opcion2);


//Resolver la operacion con "eval"

var TrueResultado = eval(operacion);


//Funcion verificacion resultado usuario

var resultado = Number(prompt(`Resuelva la siguiente operación: ${operacion}`));

function verificar() {
    if (TrueResultado === resultado) {
        return ("¡Respuesta correcta! :) ");
    } else {
        return ("¡Respuesta incorrecta! :( ");
    }

}

var verificado = verificar(TrueResultado);


//Zona de console.log y alerts

alert(verificado);
console.log(`La operación ${operacion} tiene como resultado ${TrueResultado}, el usuario introdujo ${resultado}`);

